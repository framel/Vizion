/* ********************************************************************************************** */
/*     ,o888888o.     8 888888888o.    8 8888  8888888888',8888'  ,o888888o.     b.             8 */
/*  . 8888     `88.   8 8888    `88.   8 8888         ,8',8888'. 8888     `88.   888o.          8 */
/* ,8 8888       `8b  8 8888     `88   8 8888        ,8',8888',8 8888       `8b  Y88888o.       8 */
/* 88 8888        `8b 8 8888     ,88   8 8888       ,8',8888' 88 8888        `8b .`Y888888o.    8 */
/* 88 8888         88 8 8888.   ,88'   8 8888      ,8',8888'  88 8888         88 8o. `Y888888o. 8 */
/* 88 8888         88 8 888888888P'    8 8888     ,8',8888'   88 8888         88 8`Y8o. `Y88888o8 */
/* 88 8888        ,8P 8 8888`8b        8 8888    ,8',8888'    88 8888        ,8P 8   `Y8o. `Y8888 */
/* `8 8888       ,8P  8 8888 `8b.      8 8888   ,8',8888'     `8 8888       ,8P  8      `Y8o. `Y8 */
/*  ` 8888     ,88'   8 8888   `8b.    8 8888  ,8',8888'       ` 8888     ,88'   8         `Y8o.` */
/*     `8888888P'     8 8888     `88.  8 8888 ,8',8888888888888   `8888888P'     8            `Yo */
/*                                                                                                */
/*             :::     ::: ::::::::::: ::::::::: ::::::::::: ::::::::  ::::    :::                */
/*            :+:     :+:     :+:          :+:      :+:    :+:    :+: :+:+:   :+:                 */
/*           +:+     +:+     +:+         +:+       +:+    +:+    +:+ :+:+:+  +:+                  */
/*          +#+     +:+     +#+        +#+        +#+    +#+    +:+ +#+ +:+ +#+                   */
/*          +#+   +#+      +#+       +#+         +#+    +#+    +#+ +#+  +#+#+#                    */
/*          #+#+#+#       #+#      #+#          #+#    #+#    #+# #+#   #+#+#      by FloRiZon    */
/*           ###     ########### ######### ########### ########  ###    ####       since  2019    */
/*                                                                                                */
/* ********************************************************************************************** */
/* File:          build_tree.c                                                                    */
/* Created:       04-06-2019                                                                      */
/* Description:   Building tree functions                                                         */
/* ********************************************************************************************** */

#include "vizion.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h> ////// DEBUG

void		debug_print_index(t_indices *indices, size_t addr)
{
  size_t	i;
  
  i = 0;
  printf("indices[%lu] = ", addr);
  while (i < 8)
    {
      printf("%hhu", indices[addr].index[i]);
      ++i;
    }
  printf("\n");
}

void		debug_print_indices(t_indices *indices, size_t len)
{
  size_t	i;
  size_t	j;

  i = 0;
  j = 0;
  while (i < len)
    {
      j = 0;
      printf("indices[%lu] = ", i);
      while (j < 8)
	{
	  printf("%hhu", indices[i].index[j]);
	  ++j;
	}
      printf("\n");
      ++i;
    }
}

void		debug_print_color(t_color *color)
{
  printf("rgb(%d, %d, %d)", color->red, color->green, color->blue);
}

void		get_indices(t_color *color, t_indices *indices)
{
	unsigned char	i;
	unsigned char	mask;

	i = 0;
	mask = 128;
	while (mask)
	{
	  if (color->red & mask)
	    {
	      indices->index[i] += 4;
	    }
	  if (color->green & mask)
	    {
	      indices->index[i] += 2;
	    }
	  if (color->blue & mask)
	    {
	      indices->index[i] += 1;
	    }
	  ++i;
	  mask /= 2;
	}
}

t_indices	*get_image_indices(t_rgb_image *image)
{
	size_t		i;
	size_t		image_length;
	t_indices	*indices;

	i = 0;
	image_length = image->w * image->h;
	indices = (t_indices *)malloc(sizeof(t_indices) * image_length);
	if (indices == NULL)
	{
		vz_exit("get_image_indices: Memory allocation error");
	}
	while (i < image_length)
	{
	  get_indices(&image->rgb[i], &indices[i]);
	  ++i;
	}
	return (indices);
}

size_t	vz_build_octree(t_rgb_image *image, t_tree *tree)
{
	size_t		i;
	size_t		j;
	size_t		node;
	size_t		image_length;
	t_indices	*indices;
	t_tree		*current;

	j = 0;
	node = 1;
	image_length = image->w * image->h;
	indices = get_image_indices(image);
	while (j < image_length)
	{
		i = 0;
		current = tree;
		current->references++;
		current->red += image->rgb[j].red;
		current->green += image->rgb[j].green;
		current->blue += image->rgb[j].blue;
//		while (i <= 8)
		while (i < 8)
		{
			if (current->childs == NULL)// && i != 8)
			{
				current->childs = (t_tree *)malloc(sizeof(t_tree) * 8);
				if (current->childs == NULL)
				{
					vz_exit("vz_build_tree: Memory allocation error");
				}
				bzero(current->childs, sizeof(t_tree) * 8);
				++node;
			}
//			if (i != 8)
//			{
			current = &current->childs[indices[j].index[i]];
//			}
//			else
		current->references++;
		current->red += image->rgb[j].red;
		current->green += image->rgb[j].green;
		current->blue += image->rgb[j].blue;
			++i;
		}
		++j;
	}
	current = tree;
	free(indices);
	return (node);
}
