/* ********************************************************************************************** */
/*     ,o888888o.     8 888888888o.    8 8888  8888888888',8888'  ,o888888o.     b.             8 */
/*  . 8888     `88.   8 8888    `88.   8 8888         ,8',8888'. 8888     `88.   888o.          8 */
/* ,8 8888       `8b  8 8888     `88   8 8888        ,8',8888',8 8888       `8b  Y88888o.       8 */
/* 88 8888        `8b 8 8888     ,88   8 8888       ,8',8888' 88 8888        `8b .`Y888888o.    8 */
/* 88 8888         88 8 8888.   ,88'   8 8888      ,8',8888'  88 8888         88 8o. `Y888888o. 8 */
/* 88 8888         88 8 888888888P'    8 8888     ,8',8888'   88 8888         88 8`Y8o. `Y88888o8 */
/* 88 8888        ,8P 8 8888`8b        8 8888    ,8',8888'    88 8888        ,8P 8   `Y8o. `Y8888 */
/* `8 8888       ,8P  8 8888 `8b.      8 8888   ,8',8888'     `8 8888       ,8P  8      `Y8o. `Y8 */
/*  ` 8888     ,88'   8 8888   `8b.    8 8888  ,8',8888'       ` 8888     ,88'   8         `Y8o.` */
/*     `8888888P'     8 8888     `88.  8 8888 ,8',8888888888888   `8888888P'     8            `Yo */
/*                                                                                                */
/*             :::     ::: ::::::::::: ::::::::: ::::::::::: ::::::::  ::::    :::                */
/*            :+:     :+:     :+:          :+:      :+:    :+:    :+: :+:+:   :+:                 */
/*           +:+     +:+     +:+         +:+       +:+    +:+    +:+ :+:+:+  +:+                  */
/*          +#+     +:+     +#+        +#+        +#+    +#+    +:+ +#+ +:+ +#+                   */
/*          +#+   +#+      +#+       +#+         +#+    +#+    +#+ +#+  +#+#+#                    */
/*          #+#+#+#       #+#      #+#          #+#    #+#    #+# #+#   #+#+#      by FloRiZon    */
/*           ###     ########### ######### ########### ########  ###    ####       since  2019    */
/*                                                                                                */
/* ********************************************************************************************** */
/* File:           load_image.c                                                                   */
/* Created:        04-04-2019                                                                     */
/* Description:    Load image function                                                            */
/*                                                                                                */
/* Parameters:     char *filename => Path of an image                                             */
/* Return:         A structure 't_image'                                                          */
/* ********************************************************************************************** */

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include "vizion.h"
#include <stdlib.h>

/*
t_image	make_empty_image(int w, int h, int c)
{
	t_image	out;
	
	out.data = 0;
	out.h = h;
	out.w = w;
	out.c = c;
	return (out);
}

t_image	make_image(int w, int h, int c)
{
	t_image	out;

	out = make_empty_image(w, h, c);
	out.data = (float *)malloc(h * w * c * sizeof(float));
	return (out);
}
*/

/*
**   Remember to free() img.data after use.
**   Pay attention to check alpha channel.
*/

t_image	load_image_stb(char *filename, int channels)
{
  t_image		img;
  
  img.data = stbi_load(filename, &img.w, &img.h, &img.c, channels);
  if (img.data == NULL)
    {
      fprintf(stderr, "Cannot load image \"%s\"\nSTB Reason: %s\n", filename, stbi_failure_reason());
      exit(0);
    }
  return (img);
}

/*
**   Write channel explanations
*/

t_image	vz_load_image(char *filename)
{
  t_image	out;
  
  out = load_image_stb(filename, 0);
  return (out);
}
