#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <stdint.h>

#include "vizion.h"

#define ON_INHEAP 1

typedef struct oct_node_t oct_node_t;

//oct_node_t oct_node_t;
//oct_node_t *oct_node;

// sum of all child node colors
struct oct_node_t
{
	int64_t r; // sum of all child node colors
	int64_t g; // sum of all child node colors
	int64_t b; // sum of all child node colors
	int count;
	int heap_idx;
	uint8_t n_kids;
	uint8_t kid_idx;
	uint8_t flags;
	uint8_t depth;
	oct_node_t *kids[8];
	oct_node_t *parent;
};

typedef struct
{
	int alloc;
	int n;
	oct_node_t **buf;
} node_heap;

static inline int cmp_node(oct_node_t *a, oct_node_t *b)
{
	int ac;
	int bc;

	if (a->n_kids < b->n_kids)
	{
		return -1;
	}
	if (a->n_kids > b->n_kids)
	{
		return 1;
	}
	ac = a->count >> a->depth;
	bc = b->count >> b->depth;
	return (ac < bc ? -1 : ac > bc);
}

void down_heap(node_heap *h, oct_node_t *p)
{
	int m;
	int n;

	n = p->heap_idx;
	while (1)
	{
		m = n * 2;
		if (m >= h->n)
		{
			break;
		}
		if (m + 1 < h->n && cmp_node(h->buf[m], h->buf[m + 1]) > 0)
		{
			m++;
		}
		if (cmp_node(p, h->buf[m]) <= 0)
		{
			break;
		}
		h->buf[n] = h->buf[m];
		h->buf[n]->heap_idx = n;
		n = m;
	}
	h->buf[n] = p;
	p->heap_idx = n;
}

void up_heap(node_heap *h, oct_node_t *p)
{
	int n;
	oct_node_t *prev;

	n = p->heap_idx;
	while (n > 1)
	{
		prev = h->buf[n / 2];
		if (cmp_node(p, prev) >= 0)
		{
			break;
		}
		h->buf[n] = prev;
		prev->heap_idx = n;
		n /= 2;
	}
	h->buf[n] = p;
	p->heap_idx = n;
}

void heap_add(node_heap *h, oct_node_t *p)
{
	if ((p->flags & ON_INHEAP))
	{
		down_heap(h, p);
		up_heap(h, p);
		return ;
	}
	p->flags |= ON_INHEAP;
	if (!h->n)
	{
		h->n = 1;
	}
	if (h->n >= h->alloc)
	{
		while (h->n >= h->alloc)
		{
			h->alloc += 1024;
		}
		h->buf = realloc(h->buf, sizeof(oct_node_t *) * h->alloc);
	}
	p->heap_idx = h->n;
	h->buf[h->n++] = p;
	up_heap(h, p);
}

oct_node_t *pop_heap(node_heap *h)
{
	oct_node_t *ret;

	if (h->n <= 1)
	{
		return 0;
	}
	ret = h->buf[1];
	h->buf[1] = h->buf[--h->n];
	h->buf[h->n] = 0;
	h->buf[1]->heap_idx = 1;
	down_heap(h, h->buf[1]);
	return (ret);
}

static oct_node_t *pool = 0;

oct_node_t *node_new(uint8_t idx, uint8_t depth, oct_node_t *p)
{
	static int len = 0;
	oct_node_t *x;
	oct_node_t *ptr;

	if (len <= 1)
	{
		ptr = calloc(sizeof(oct_node_t), 2048);
		ptr->parent = pool;
		pool = ptr;
		len = 2047;
	}
	x = pool + len--;
	x->kid_idx = idx;
	x->depth = depth;
	x->parent = p;
	if (p)
	{
		p->n_kids++;
	}
	return (x);
}

void node_free(void)
{
	oct_node_t *p;

	while (pool)
	{
		p = pool->parent;
		free(pool);
		pool = p;
	}
}

oct_node_t *node_insert(oct_node_t *root, uint8_t *data)
{
	uint8_t i;
	uint8_t bit;
	uint8_t depth;

	depth = 0;
	for (bit = 1 << 7; ++depth < 8; bit >>= 1)
	{
		i = !!(data[1] & bit) * 4 + !!(data[0] & bit) * 2 + !!(data[2] & bit);
		if (!root->kids[i])
		{
			root->kids[i] = node_new(i, depth, root);
		}
		root = root->kids[i];
	}
	root->r += data[0];
	root->g += data[1];
	root->b += data[2];
	root->count++;
	return (root);
}

oct_node_t *node_fold(oct_node_t *p)
{
	oct_node_t *q;

	if (p->n_kids)
	{
		abort();
	}
	q = p->parent;
	q->count += p->count;
	q->r += p->r;
	q->g += p->g;
	q->b += p->b;
	q->n_kids --;
	q->kids[p->kid_idx] = 0;
	return (q);
}
/*
void color_replace(oct_node_t *root, uint8_t *data)
{
	uint8_t i;
	uint8_t bit;

	for (bit = 1 << 7; bit; bit >>= 1)
	{
		i = !!(data[1] & bit) * 4 + !!(data[0] & bit) * 2 + !!(data[2] & bit);
		if (!root->kids[i])
		{
			break;
		}
		root = root->kids[i];
	}
	data[0] = root->r;
	data[1] = root->g;
	data[2] = root->b;
}*/
/*
void error_diffuse(t_image *im, node_heap *h)
{
	oct_node_t *nearest_color(int *v) {
		int i;
		int diff, max = 100000000;
		oct_node_t *o = 0;
		for (i = 1; i < h->n; i++) {
			diff =  3 * abs(h->buf[i]->r - v[0])
				+ 5 * abs(h->buf[i]->g - v[1])
				+ 2 * abs(h->buf[i]->b - v[2]);
			if (diff < max) {
				max = diff;
				o = h->buf[i];
			}
		}
		return o;
	}

#define POS(i, j) (3 * ((i) * im->w + (j)))
	int i, j;
	int *npx = calloc(sizeof(int), im->h * im->w * 3), *px;
	int v[3];
	unsigned char *data = im->data;
	oct_node_t *nd;

#define C10 7
#define C01 5
#define C11 2
#define C00 1
#define CTOTAL (C00 + C11 + C10 + C01)

	for (px = npx, i = 0; i < im->h; i++) {
		for (j = 0; j < im->w; j++, data += 3, px += 3) {
			px[0] = (int)data[0] * CTOTAL;
			px[1] = (int)data[1] * CTOTAL;
			px[2] = (int)data[2] * CTOTAL;
		}
	}
#define clamp(x, i) if (x[i] > 255) x[i] = 255; if (x[i] < 0) x[i] = 0
	data = im->data;
	for (px = npx, i = 0; i < im->h; i++) {
		for (j = 0; j < im->w; j++, data += 3, px += 3) {
			px[0] /= CTOTAL;
			px[1] /= CTOTAL;
			px[2] /= CTOTAL;
			clamp(px, 0); clamp(px, 1); clamp(px, 2);
			nd = nearest_color(px);
			v[0] = px[0] - nd->r;
			v[1] = px[1] - nd->g;
			v[2] = px[2] - nd->b;
			data[0] = nd->r; data[1] = nd->g; data[2] = nd->b;
			if (j < im->w - 1) {
				npx[POS(i, j+1) + 0] += v[0] * C10;
				npx[POS(i, j+1) + 1] += v[1] * C10;
				npx[POS(i, j+1) + 2] += v[2] * C10;
			}
			if (i >= im->h - 1) continue;

			npx[POS(i+1, j) + 0] += v[0] * C01;
			npx[POS(i+1, j) + 1] += v[1] * C01;
			npx[POS(i+1, j) + 2] += v[2] * C01;

			if (j < im->w - 1) {
				npx[POS(i+1, j+1) + 0] += v[0] * C11;
				npx[POS(i+1, j+1) + 1] += v[1] * C11;
				npx[POS(i+1, j+1) + 2] += v[2] * C11;
			}
			if (j) {
				npx[POS(i+1, j-1) + 0] += v[0] * C00;
				npx[POS(i+1, j-1) + 1] += v[1] * C00;
				npx[POS(i+1, j-1) + 2] += v[2] * C00;
			}
		}
	}
	free(npx);
}

void color_quant(t_image *im, int n_colors, int dither)
{
	int i;
	double c;
	unsigned char *data = im->data;
	node_heap heap = { 0, 0, 0 };
	oct_node_t *got;
	oct_node_t *root = node_new(0, 0, 0);

	for (i = 0; i < im->w * im->h; i++, data += 3)
		heap_add(&heap, node_insert(root, data));

	while (heap.n > n_colors + 1)
		heap_add(&heap, node_fold(pop_heap(&heap)));

	for (i = 1; i < heap.n; i++) {
		got = heap.buf[i];
		c = got->count;
		got->r = got->r / c + .5;
		got->g = got->g / c + .5;
		got->b = got->b / c + .5;
	}

	if (dither)
	{
		error_diffuse(im, &heap);
	}
	else
	{
		for (i = 0, data = im->data; i < im->w * im->h; i++, data += 3)
		{
			color_replace(root, data);
		}
	}
	node_free();
	free(heap.buf);
}
*/
void color_quant(t_image *im, int n_colors)
{
	int i;
	int im_size;
	double c;
	uint8_t *data;
	oct_node_t *root;
	oct_node_t *got;
	node_heap heap = { 0, 0, 0 };

	data = im->data;
	im_size = im->w * im->h;
	root = node_new(0, 0, 0);
	for (i = 0; i < im_size; i++, data += 3)
	{
		heap_add(&heap, node_insert(root, data));
	}
	while (heap.n > n_colors + 1)
	{
		heap_add(&heap, node_fold(pop_heap(&heap)));
	}
	for (i = 1; i < heap.n; i++)
	{
		got = heap.buf[i];
		c = got->count;
		got->r = got->r / c + .5;
		got->g = got->g / c + .5;
		got->b = got->b / c + .5;
		printf("%2d | %3lu %3lu %3lu (%d pixels = %.2f%%)\n",
			   i, got->r, got->g, got->b, got->count, got->count / (float)im_size * 100);
	}
/*	for (i = 0, data = im->data; i < im->w * im->h; i++, data += 3)
	{
		color_replace(root, data);
	}
*/	node_free();
	free(heap.buf);
}

int main(int argc, char **argv)
{
	int n_colors;
	int quality;

	if (argc < 3)
	{
//		fprintf(stderr, "usage: %s <input(jpg)> [n_colors] [output quality(1-100)]\n", argv[0]);
		fprintf(stderr, "usage: %s <input(jpg)> [n_colors]\n", argv[0]);
		return (0);
	}
	n_colors = atoi(argv[2]) ? : 16; // GCC extension
	t_image im = vz_load_image(argv[1]);
//	color_quant(&im, n_colors, 1); // Dither
	color_quant(&im, n_colors);
/*	if (argc > 4)
	{
		quality = atoi(argv[4]) ? : 100;
		vz_save_image(im, argv[3], quality);
	}
*/	free(im.data);
	return (0);
}
