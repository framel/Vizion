/* ********************************************************************************************** */
/*     ,o888888o.     8 888888888o.    8 8888  8888888888',8888'  ,o888888o.     b.             8 */
/*  . 8888     `88.   8 8888    `88.   8 8888         ,8',8888'. 8888     `88.   888o.          8 */
/* ,8 8888       `8b  8 8888     `88   8 8888        ,8',8888',8 8888       `8b  Y88888o.       8 */
/* 88 8888        `8b 8 8888     ,88   8 8888       ,8',8888' 88 8888        `8b .`Y888888o.    8 */
/* 88 8888         88 8 8888.   ,88'   8 8888      ,8',8888'  88 8888         88 8o. `Y888888o. 8 */
/* 88 8888         88 8 888888888P'    8 8888     ,8',8888'   88 8888         88 8`Y8o. `Y88888o8 */
/* 88 8888        ,8P 8 8888`8b        8 8888    ,8',8888'    88 8888        ,8P 8   `Y8o. `Y8888 */
/* `8 8888       ,8P  8 8888 `8b.      8 8888   ,8',8888'     `8 8888       ,8P  8      `Y8o. `Y8 */
/*  ` 8888     ,88'   8 8888   `8b.    8 8888  ,8',8888'       ` 8888     ,88'   8         `Y8o.` */
/*     `8888888P'     8 8888     `88.  8 8888 ,8',8888888888888   `8888888P'     8            `Yo */
/*                                                                                                */
/*             :::     ::: ::::::::::: ::::::::: ::::::::::: ::::::::  ::::    :::                */
/*            :+:     :+:     :+:          :+:      :+:    :+:    :+: :+:+:   :+:                 */
/*           +:+     +:+     +:+         +:+       +:+    +:+    +:+ :+:+:+  +:+                  */
/*          +#+     +:+     +#+        +#+        +#+    +#+    +:+ +#+ +:+ +#+                   */
/*          +#+   +#+      +#+       +#+         +#+    +#+    +#+ +#+  +#+#+#                    */
/*          #+#+#+#       #+#      #+#          #+#    #+#    #+# #+#   #+#+#      by FloRiZon    */
/*           ###     ########### ######### ########### ########  ###    ####       since  2019    */
/*                                                                                                */
/* ********************************************************************************************** */
/* File:          image.c                                                                         */
/* Created:       04-06-2019                                                                      */
/* Description:   Image manipulation functions                                                    */
/* ********************************************************************************************** */

#include "vizion.h"
#include <stdlib.h>

t_rgb_image	*vz_plain_to_rgb(t_image *image)
{
	size_t	i;
	size_t	image_length;
	t_rgb_image	*rgb_image;

	if (image->c != 3)
	{
		vz_exit("vz_plain_to_rgb: Invalid image format.");
	}
	rgb_image = (t_rgb_image *)malloc(sizeof(t_rgb_image));
	if (rgb_image == NULL)
	{
		vz_exit("vz_plain_to_rgb: Memory allocation error");
	}
	rgb_image->w = image->w;
	rgb_image->h = image->h;
	rgb_image->rgb = (t_color *)malloc(sizeof(t_color) * rgb_image->w * rgb_image->h);
	if (rgb_image->rgb == NULL)
	{
		vz_exit("vz_plain_to_rgb: Memory allocation error");
	}
	i = 0;
	image_length = image->w * image->h;
	while (i < image_length)
	{
		rgb_image->rgb[i].red = image->data[i];
		rgb_image->rgb[i].green = image->data[i + image_length];
		rgb_image->rgb[i].blue = image->data[i + image_length + image_length];
		++i;
	}
	return (rgb_image);
}
