/* ********************************************************************************************** */
/*     ,o888888o.     8 888888888o.    8 8888  8888888888',8888'  ,o888888o.     b.             8 */
/*  . 8888     `88.   8 8888    `88.   8 8888         ,8',8888'. 8888     `88.   888o.          8 */
/* ,8 8888       `8b  8 8888     `88   8 8888        ,8',8888',8 8888       `8b  Y88888o.       8 */
/* 88 8888        `8b 8 8888     ,88   8 8888       ,8',8888' 88 8888        `8b .`Y888888o.    8 */
/* 88 8888         88 8 8888.   ,88'   8 8888      ,8',8888'  88 8888         88 8o. `Y888888o. 8 */
/* 88 8888         88 8 888888888P'    8 8888     ,8',8888'   88 8888         88 8`Y8o. `Y88888o8 */
/* 88 8888        ,8P 8 8888`8b        8 8888    ,8',8888'    88 8888        ,8P 8   `Y8o. `Y8888 */
/* `8 8888       ,8P  8 8888 `8b.      8 8888   ,8',8888'     `8 8888       ,8P  8      `Y8o. `Y8 */
/*  ` 8888     ,88'   8 8888   `8b.    8 8888  ,8',8888'       ` 8888     ,88'   8         `Y8o.` */
/*     `8888888P'     8 8888     `88.  8 8888 ,8',8888888888888   `8888888P'     8            `Yo */
/*                                                                                                */
/*             :::     ::: ::::::::::: ::::::::: ::::::::::: ::::::::  ::::    :::                */
/*            :+:     :+:     :+:          :+:      :+:    :+:    :+: :+:+:   :+:                 */
/*           +:+     +:+     +:+         +:+       +:+    +:+    +:+ :+:+:+  +:+                  */
/*          +#+     +:+     +#+        +#+        +#+    +#+    +:+ +#+ +:+ +#+                   */
/*          +#+   +#+      +#+       +#+         +#+    +#+    +#+ +#+  +#+#+#                    */
/*          #+#+#+#       #+#      #+#          #+#    #+#    #+# #+#   #+#+#      by FloRiZon    */
/*           ###     ########### ######### ########### ########  ###    ####       since  2019    */
/*                                                                                                */
/* ********************************************************************************************** */
/* File:          vizion.c                                                                        */
/* Created:       04-04-2019                                                                      */
/* Description:   Image recognition software                                                      */
/* ********************************************************************************************** */

/*
 **		DATA FLOW
 **		=========
 ** 
 ** image.jpg	--> vz_load_img()
 **				--> build_octree()
 **				--> browse_octree()
 **				--> get_color()
 **				--> build_palette()	--> palette.jpg
 ** 
*/	

#include "vizion.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void	vz_welcome(void)
{
  printf("\n====================================\n+               VIZION             +\n====================================\n                           by ORIZON\n\n");
}

void	vz_exit(char *message)
{
  if (message)
    {
      printf("%s\n", message);
    }
  printf("See you soon.\n");
  exit(0);
}

void	vz_test_image(char *filename)
{
  int			i;
  t_image		image;
  t_rgb_image	*rgb_image;
  t_tree		*tree;
  t_tree		**node_list;
  size_t		n_nodes;
  t_colors		*colors_list;
//  void		**alloc_list;
  
  image = vz_load_image(filename);
  rgb_image = vz_plain_to_rgb(&image);
  tree = (t_tree *)malloc(sizeof(t_tree));
  if (tree == NULL)
  {
	  vz_exit("vz_test_image: Memory allocation error");
  }
  bzero(tree, sizeof(t_tree));
  printf("DEBUG 1\n");
  n_nodes = vz_build_octree(rgb_image, tree);
/*  alloc_list = (void **)malloc(sizeof(void *) * (n_nodes + 1));
  if (alloc_list == NULL)
  {
	  vz_exit("vz_test_image: Memory allocation error");
  }
  bzero(alloc_list, sizeof(void *) * (n_nodes + 1));
*/  //  node_list = vz_list_node(tree, n_nodes); //// WARNING: SOME SEGFAULT
  //  n_nodes = vz_build_octree(rgb_image, tree);
  printf("DEBUG 2\n");
  node_list = vz_list_node(tree, n_nodes);//, alloc_list); //// WARNING: SOME SEGFAULT
//  printf("DEBUG 2: refnode -> %lu\n", (*node_list)->references);
  printf("DEBUG 3\n");
  colors_list = vz_get_colors(node_list, n_nodes, 16); //// WARNING: MAY SEGFAULT HERE TOO
//  printf("DEBUG 3.1: col -> %hhu\n", colors_list->color.red);
  printf("DEBUG 4\n");
  vz_print_colors(colors_list);
//  vz_save_image(image, "test.jpg", 1);
//  vz_print_result();
//  vz_print_palette();
  free(colors_list);
  vz_free_tree(node_list);
//  vz_free_tree(alloc_list);
  free(node_list);
//  free(alloc_list);
  free(rgb_image->rgb);
  printf("DEBUG 5\n");
  free(rgb_image);
  printf("DEBUG 6\n");
  free(image.data);
//  free(tree);
}

void	vz_test(int argc, char **argv)
{
  if (argc < 2)
    {
      vz_exit("No input.\n");
    }
  vz_test_image(argv[1]);
}

int	main(int argc, char **argv)
{
  vz_welcome();
  vz_test(argc, argv);
//  vz_exit("All right,");
  while (1)
	  ;
  return (0);
}
