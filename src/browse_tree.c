/* ********************************************************************************************** */
/*     ,o888888o.     8 888888888o.    8 8888  8888888888',8888'  ,o888888o.     b.             8 */
/*  . 8888     `88.   8 8888    `88.   8 8888         ,8',8888'. 8888     `88.   888o.          8 */
/* ,8 8888       `8b  8 8888     `88   8 8888        ,8',8888',8 8888       `8b  Y88888o.       8 */
/* 88 8888        `8b 8 8888     ,88   8 8888       ,8',8888' 88 8888        `8b .`Y888888o.    8 */
/* 88 8888         88 8 8888.   ,88'   8 8888      ,8',8888'  88 8888         88 8o. `Y888888o. 8 */
/* 88 8888         88 8 888888888P'    8 8888     ,8',8888'   88 8888         88 8`Y8o. `Y88888o8 */
/* 88 8888        ,8P 8 8888`8b        8 8888    ,8',8888'    88 8888        ,8P 8   `Y8o. `Y8888 */
/* `8 8888       ,8P  8 8888 `8b.      8 8888   ,8',8888'     `8 8888       ,8P  8      `Y8o. `Y8 */
/*  ` 8888     ,88'   8 8888   `8b.    8 8888  ,8',8888'       ` 8888     ,88'   8         `Y8o.` */
/*     `8888888P'     8 8888     `88.  8 8888 ,8',8888888888888   `8888888P'     8            `Yo */
/*                                                                                                */
/*             :::     ::: ::::::::::: ::::::::: ::::::::::: ::::::::  ::::    :::                */
/*            :+:     :+:     :+:          :+:      :+:    :+:    :+: :+:+:   :+:                 */
/*           +:+     +:+     +:+         +:+       +:+    +:+    +:+ :+:+:+  +:+                  */
/*          +#+     +:+     +#+        +#+        +#+    +#+    +:+ +#+ +:+ +#+                   */
/*          +#+   +#+      +#+       +#+         +#+    +#+    +#+ +#+  +#+#+#                    */
/*          #+#+#+#       #+#      #+#          #+#    #+#    #+# #+#   #+#+#      by FloRiZon    */
/*           ###     ########### ######### ########### ########  ###    ####       since  2019    */
/*                                                                                                */
/* ********************************************************************************************** */
/* File:          browse_tree.c                                                                   */
/* Created:       04-06-2019                                                                      */
/* Description:   Tree manipulation functions                                                     */
/* ********************************************************************************************** */

#include "vizion.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h> /// DEBUG

void		vz_free_tree(t_tree **node_list)
//void		vz_free_tree(void **node_list)
{
	size_t	i;

	i = 1;
	while (node_list[i])
	{
		free(node_list[i]);
		++i;
	}
}

void		debug(t_tree	**node_list, int len)
{
	int i;

	i = 0;
	while(i++ < len)
	  {
	    printf("DEBUG 2: ptr -> %p\n", node_list[i]);
	  }
}

t_tree		**vz_list_node(t_tree *tree, size_t node)//, void **alloc_list)
{
  size_t	i;
  size_t	j;
  size_t	k;
//  size_t	l;
  t_tree	**node_list;
  //  t_tree	**alloc_list;
  
  j = 1;
  k = 0;
//  l = 1;
  node_list = (t_tree **)malloc(sizeof(t_tree *) * (node + 1));
  if (node_list == NULL)
    {
      vz_exit("vz_list_node: Memory allocation error");
    }
  bzero(node_list, sizeof(t_tree *) * (node + 1));
  node_list[0] = tree;
  while (node_list[k] && j < node - 1) // DEBUG
    {
      //      printf("DEBUG i = %lu\n", i);
      /*      printf("DEBUG k = %lu\n", k);
      printf("DEBUG j = %lu\n", j);
      printf("DEBUG node = %lu\n", node);
      */      i = 0;
      while (i < 8)
	{
	  //			if (current->childs && current->childs[i].references)
	  if (node_list[k]->childs[i].references)
	    {
/*	      if (node_list[k]->childs != alloc_list[l - 1])
		{
		  alloc_list[l] = node_list[k]->childs;
		  ++l;
		  }
*/	      node_list[j] = &node_list[k]->childs[i];
	      ++j;
	    }
	  ++i;
	}
      //		current = node_list[k];
      ++k;
    }
  //  debug(node_list, 2);
  return (node_list);
}

t_colors	*vz_get_colors(t_tree **node_list, size_t n_nodes, unsigned char depth)
{
	size_t			i;
	size_t			j;
	size_t			acc;
	size_t			n_pixels;
	t_tree			*current;
	t_colors		*colors_list;

	i = 0;
	j = 0;
	n_pixels = (*node_list)->references;
	if (n_pixels == 0)
	{
		vz_exit("Error: Empty image");
	}
	colors_list = (t_colors *)malloc(sizeof(t_colors) * n_nodes);
	if (colors_list == NULL)
	{
		vz_exit("vz_get_color: Memory allocation error");
	}
	bzero(colors_list, sizeof(t_colors) * n_nodes);
	while (depth)
	{
		acc = 0;
		while (node_list[i] && acc < n_pixels)
		{
			current = node_list[i];
			acc += current->references;
			colors_list[i].color.red = current->red;
			colors_list[i].color.green = current->green;
			colors_list[i].color.blue = current->blue;
			colors_list[i].weight = current->references / (float)n_pixels;
//			if (i == 0)
//				printf("DEBUG: red - > %d\n", colors_list[i].color.red);
			i++;//= 800;
		}
		--depth;
	}
	return (colors_list);
}
