/* ********************************************************************************************** */
/*     ,o888888o.     8 888888888o.    8 8888  8888888888',8888'  ,o888888o.     b.             8 */
/*  . 8888     `88.   8 8888    `88.   8 8888         ,8',8888'. 8888     `88.   888o.          8 */
/* ,8 8888       `8b  8 8888     `88   8 8888        ,8',8888',8 8888       `8b  Y88888o.       8 */
/* 88 8888        `8b 8 8888     ,88   8 8888       ,8',8888' 88 8888        `8b .`Y888888o.    8 */
/* 88 8888         88 8 8888.   ,88'   8 8888      ,8',8888'  88 8888         88 8o. `Y888888o. 8 */
/* 88 8888         88 8 888888888P'    8 8888     ,8',8888'   88 8888         88 8`Y8o. `Y88888o8 */
/* 88 8888        ,8P 8 8888`8b        8 8888    ,8',8888'    88 8888        ,8P 8   `Y8o. `Y8888 */
/* `8 8888       ,8P  8 8888 `8b.      8 8888   ,8',8888'     `8 8888       ,8P  8      `Y8o. `Y8 */
/*  ` 8888     ,88'   8 8888   `8b.    8 8888  ,8',8888'       ` 8888     ,88'   8         `Y8o.` */
/*     `8888888P'     8 8888     `88.  8 8888 ,8',8888888888888   `8888888P'     8            `Yo */
/*                                                                                                */
/*             :::     ::: ::::::::::: ::::::::: ::::::::::: ::::::::  ::::    :::                */
/*            :+:     :+:     :+:          :+:      :+:    :+:    :+: :+:+:   :+:                 */
/*           +:+     +:+     +:+         +:+       +:+    +:+    +:+ :+:+:+  +:+                  */
/*          +#+     +:+     +#+        +#+        +#+    +#+    +:+ +#+ +:+ +#+                   */
/*          +#+   +#+      +#+       +#+         +#+    +#+    +#+ +#+  +#+#+#                    */
/*          #+#+#+#       #+#      #+#          #+#    #+#    #+# #+#   #+#+#      by FloRiZon    */
/*           ###     ########### ######### ########### ########  ###    ####       since  2019    */
/*                                                                                                */
/* ********************************************************************************************** */
/* File:          Tree.h                                                                          */
/* Created:       04-06-2019                                                                      */
/* Description:   Defining tree structure                                                         */
/* ********************************************************************************************** */

#ifndef TREE_H
# define TREE_H

typedef struct	s_tree
{
	unsigned long	references;
	unsigned long	red;
	unsigned long	green;
	unsigned long	blue;
	struct s_tree	*childs;
}				t_tree;

#endif
