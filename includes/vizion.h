/* ********************************************************************************************** */
/*     ,o888888o.     8 888888888o.    8 8888  8888888888',8888'  ,o888888o.     b.             8 */
/*  . 8888     `88.   8 8888    `88.   8 8888         ,8',8888'. 8888     `88.   888o.          8 */
/* ,8 8888       `8b  8 8888     `88   8 8888        ,8',8888',8 8888       `8b  Y88888o.       8 */
/* 88 8888        `8b 8 8888     ,88   8 8888       ,8',8888' 88 8888        `8b .`Y888888o.    8 */
/* 88 8888         88 8 8888.   ,88'   8 8888      ,8',8888'  88 8888         88 8o. `Y888888o. 8 */
/* 88 8888         88 8 888888888P'    8 8888     ,8',8888'   88 8888         88 8`Y8o. `Y88888o8 */
/* 88 8888        ,8P 8 8888`8b        8 8888    ,8',8888'    88 8888        ,8P 8   `Y8o. `Y8888 */
/* `8 8888       ,8P  8 8888 `8b.      8 8888   ,8',8888'     `8 8888       ,8P  8      `Y8o. `Y8 */
/*  ` 8888     ,88'   8 8888   `8b.    8 8888  ,8',8888'       ` 8888     ,88'   8         `Y8o.` */
/*     `8888888P'     8 8888     `88.  8 8888 ,8',8888888888888   `8888888P'     8            `Yo */
/*                                                                                                */
/*             :::     ::: ::::::::::: ::::::::: ::::::::::: ::::::::  ::::    :::                */
/*            :+:     :+:     :+:          :+:      :+:    :+:    :+: :+:+:   :+:                 */
/*           +:+     +:+     +:+         +:+       +:+    +:+    +:+ :+:+:+  +:+                  */
/*          +#+     +:+     +#+        +#+        +#+    +#+    +:+ +#+ +:+ +#+                   */
/*          +#+   +#+      +#+       +#+         +#+    +#+    +#+ +#+  +#+#+#                    */  
/*          #+#+#+#       #+#      #+#          #+#    #+#    #+# #+#   #+#+#      by FloRiZon    */
/*           ###     ########### ######### ########### ########  ###    ####       since  2019    */
/*                                                                                                */
/* ********************************************************************************************** */
/* File:          vizion.h                                                                        */
/* Created:       04-04-2019                                                                      */
/* Description:   Image recognition software                                                      */
/* ********************************************************************************************** */

#ifndef VIZION_H
# define VIZION_H

# include "image.h"
# include "tree.h"
# include <stdlib.h>

/*
**	IMAGE.C
*/

t_rgb_image	*vz_plain_to_rgb(t_image *image);

/*
**	PRINT_COLORS.C
*/

void		vz_print_colors(t_colors *colors);

/*
**	LOAD_IMAGE.C
*/

t_image		vz_load_image(char *filename);

/*
**	BUILD_TREE.C
*/

size_t		vz_build_octree(t_rgb_image *image, t_tree *tree);

/*
**	BROWSE_TREE.C
*/

t_tree		**vz_list_node(t_tree *tree, size_t node);
//t_tree		**vz_list_node(t_tree *tree, size_t node, void **alloc_list);
t_colors	*vz_get_colors(t_tree **node_list, size_t n_nodes, unsigned char depth);
void		vz_free_tree(t_tree **node_list);
//void		vz_free_tree(void **node_list);

/*
**	SAVE_IMAGE.C
*/

void		vz_save_image(t_image img, const char *name, int quality);

/*
**	VIZION.C
*/

void		vz_exit(char *message);

#endif
